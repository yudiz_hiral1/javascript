
const user = {
    name: "Your Name",
    address: {
      personal: {
        line1: "101",
        line2: "street Line",
        city: "NY",
        state: "WX",
        coordinates: {
          x: 35.12,
          y: -21.49,
        },
      },
      office: {
        city: "City",
        state: "WX",
        area: {
          landmark: "landmark",
        },
        coordinates: {
          x: 35.12,
          y: -21.49,
        },
      },
    },
    contact: {
      phone: {
        home: "xxx",
        office: "yyy",
      },
      other: {
        fax: '234'
      },
      email: {
        home: "xxx",
        office: "yyy",
      },
    },
  };

const getResultObj = {}

let reducer = (obj,name) => 
{
    for (const i in obj)
    {
        if(typeof obj[i] == 'object')
        {
            reducer(obj[i],name+"_" + i)
        }
        else
        {
            getResultObj[name+ "_" +i] = obj[i]
            }
       
    }
}
reducer(user, 'user')
console.log(getResultObj);