const img1 =
  "https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/house-plant-display-beside-window-indoor-plants-royalty-free-image-839958456-1563398079.jpg";
const img2 =
  "https://images.pexels.com/photos/3076899/pexels-photo-3076899.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1";
const img3 =
"https://images.pexels.com/photos/793012/pexels-photo-793012.jpeg?auto=compress&cs=tinysrgb&w=400"
const img4 =
  "https://assets.hongkiat.com/uploads/100-absolutely-beautiful-nature-wallpapers-for-your-desktop/blue-sea-sunset.jpg";
const img5 =
 "https://cdn.pixabay.com/photo/2018/07/30/10/29/himalayas-3572246__340.jpg";
const img6 =
  "http://2.bp.blogspot.com/-M3ou-DRPHt4/T-aCem7r-6I/AAAAAAAAAV8/ZskbNUyhRHg/s1600/11579.jpg";
const img7 =
  "https://3.imimg.com/data3/ED/VM/MY-1941440/kerala-nature-wildlife-tour-500x500.jpg";
const img8 =
  "https://t3.ftcdn.net/jpg/03/58/85/26/360_F_358852600_Ri5XgkM2N5Pu4Jom0aeqQNhyXXKaFNeW.webp";
const img9 =
  "https://i.pinimg.com/originals/6a/8f/f1/6a8ff1b38132735919cae08d86fcc743.jpg";
const img10 =
  "https://wallpaperim.net/_data/i/upload/2014/09/22/20140922742672-08b617a3-la.jpg";


function playInterval() {
  let i = 0;
  const pics = [img1, img2, img3, img4, img5, img6, img7, img8, img9, img10];
  const intervalValue = document.getElementById("interval-val").value;
  const el = document.getElementsByTagName("img")[0];
  
  function setTimer() {
    el.src = pics[i]; // set the image
    i = (i + 1) % pics.length; // update the counter
  }
  setInterval(setTimer, intervalValue);
}
